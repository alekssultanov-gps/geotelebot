"""
    version="1.0.0" 
    Модуль для инициализации с телеграмм бота saleksgeoinfobot
    Инфобот для различной информации для походов и путешествий.

"""
from aiogram import Bot, Dispatcher, executor, types
import router as rt

API_TOKEN  = rt.api_token()

# Инициализация бота
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

@dp.message_handler(commands='start') 
async def send_welcome(message: types.Message):
   await rt.send_welcome(message)

# Меню
@dp.message_handler(commands='menu') 
async def send_menu(message: types.Message):
   await rt.send_menu(message) 

# Прогноз погоды   
@dp.callback_query_handler(text='weathers')
async def send_weathers(callback: types.CallbackQuery):
   await rt.send_weathers(callback)

# Карты список
@dp.callback_query_handler(text='maps')
async def send_maps(callback: types.CallbackQuery):
   await rt.send_maps(callback)


# Получение карты 
@dp.callback_query_handler(text_startswith='getMaps_')
async def get_map(callback: types.CallbackQuery):
   await rt.get_map(callback)


# Запуск
if __name__ == '__main__':
   executor.start_polling(dp, skip_updates=True)