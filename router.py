""" 
   version="1.0.0" 
   Модуль обработки запросов телеграмм бота saleksgeoinfobot

   """
from aiogram import types
from dotenv import load_dotenv
from datetime import datetime

import os

#Загрузка переменных
def load_env():
    dotenv_path = os.path.join(os.getcwd(),'.env')
    if os.path.exists(dotenv_path):
        load_dotenv(dotenv_path)

def api_token():
   load_env()
   return os.getenv("API_TOKEN", "0")


# Логирование
def printlog(tiile,message):
   _date = str(datetime.now())
   msg = f"{_date}    {tiile}    ,id пользователя: {message.from_user.id} ,username: {message.from_user.username} ,полное имя: {message.from_user.full_name}"
   print(msg)

# Список карт из каталога
def getMapListDirectory():
    maps  = []
    sRes  = 0
    _dirName = os.getenv("MAP_DIR", ".")
    mapname = os.getenv("MAP_NAME")
    lstdir  = os.listdir(_dirName) 
    for l in lstdir:
        if l.lower().find(mapname) == 0 and l.lower().find('mapdemo') == -1 and l.lower().find('maptest') == -1 and l[-4::].lower() == 'html':
            sRes +=1
            maps.append(l[3:-5].strip())
    return sRes, maps

# Настроим приветственное окно для нового пользователя
async def send_welcome(message: types.Message):
  usnameTxt =""
  if message.from_user.username != None and message.from_user.full_name != None:
     usnameTxt= f", {message.from_user.full_name} ({message.from_user.username})"
  if message.from_user.username != None and message.from_user.full_name == None:
     usnameTxt= ', '+message.from_user.username
  if message.from_user.username == None and message.from_user.full_name != None:
     usnameTxt= ', '+message.from_user.full_name
      
  titlemsg = f"Добро пожаловать{usnameTxt}!\nЭто инфобот для различной информации для походов и путешествий"
  printlog("Приветствие",message)
  await message.answer(titlemsg) 

# Меню
async def send_menu(message: types.Message):
  km = types.InlineKeyboardMarkup(row_width=2)
  km.add(types.InlineKeyboardButton(text="Карты походов", callback_data="maps"),
         types.InlineKeyboardButton(text="Прогнозы погоды", callback_data="weathers"))
   
  titlemsg = "<b>Меню</b>"
  printlog("Меню",message)
  await message.answer(titlemsg,parse_mode="html", reply_markup=km) 

# Прогноз погоды -ссылки  
async def send_weathers(callback: types.CallbackQuery):
  urlkb = types.InlineKeyboardMarkup(row_width=3)
  urlButton1 = types.InlineKeyboardButton(text='Ташкент', url='https://www.windy.com/41.329/69.341/meteogram?41.328,69.341,18,m:eN1ah4J')
  urlButton2 = types.InlineKeyboardButton(text='Аксаката',url='https://www.windy.com/41.440/69.854/meteogram?41.439,69.854,18,m:eOfah5A')
  urlButton3 = types.InlineKeyboardButton(text='Амирсай', url='https://www.windy.com/41.468/69.954/meteogram?41.467,69.954,18,m:eOhah5K')
  urlButton4 = types.InlineKeyboardButton(text='Арашанские озера', url='https://www.windy.com/41.365/70.516/meteogram?41.364,70.516,18,m:eN5ah6G')
  urlButton5 = types.InlineKeyboardButton(text='Бадак', url='https://www.windy.com/41.857/70.428/meteogram?41.856,70.428,18,m:eOUah6x')
  urlButton6 = types.InlineKeyboardButton(text='Пальтау', url='https://www.windy.com/41.573/70.146/meteogram?41.572,70.146,18,m:eOsah53')
  urlButton7 = types.InlineKeyboardButton(text='Таваксай', url='https://www.windy.com/41.614/69.642/meteogram?41.613,69.642,18,m:eOwah5f')
  urlButton8 = types.InlineKeyboardButton(text='Сукок', url='https://www.windy.com/41.248/69.781/meteogram?41.613,69.642,18')
  urlButton9 = types.InlineKeyboardButton(text='Лашкерек', url='https://www.windy.com/41.248/69.781/meteogram?41.613,69.642,18')
  urlkb.add(urlButton1,urlButton2,urlButton3)  
  urlkb.add(urlButton4,urlButton5,urlButton6)  
  urlkb.add(urlButton7,urlButton8,urlButton9)  
  printlog("Прогноз погоды",callback)
  await callback.message.answer('погода', reply_markup=urlkb)
  await callback.answer() 

# Карты  - список
async def send_maps(callback: types.CallbackQuery):
  printlog("Карты",callback)
  _res, _maps = getMapListDirectory()
  if _res == 0:
    await callback.answer(
           text="Карты не найдены",
           show_alert=True
       ) 
  else:  
    urlkb = types.InlineKeyboardMarkup(row_width=2)
    listkb = []
    for _m in _maps:
        listkb.append(types.InlineKeyboardButton(text=_m, callback_data="getMaps_"+_m))
    urlkb.add(*listkb)    
    await callback.message.answer('Выберете карту из списка:', reply_markup=urlkb)
    await callback.answer() 

# Получение карты
async def get_map(callback: types.CallbackQuery):
   printlog("Запрос на получение карты "+callback.data[8:],callback) 
   _dirName = os.getenv("MAP_DIR", ".")
   mapname = os.getenv("MAP_NAME")
   filename = os.path.join(_dirName,mapname+callback.data[8:]+'.html')
   with open(filename, 'rb') as file:
        await callback.message.answer_document(file)
   printlog("Получена карта "+callback.data[8:],callback)      
   await callback.answer()   
